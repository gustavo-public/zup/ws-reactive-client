FROM maven:3.6.1-jdk-12 as builder

ADD . /tmp

WORKDIR /tmp

RUN mvn clean package  && \
	chmod -R +x /tmp/target/ws-reactive-client-0.0.1-SNAPSHOT.jar

RUN jlink \
    --add-modules \
        java.base,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument \
    --compress 2 \
    --strip-debug \
    --no-header-files \
    --no-man-pages \
    --output /opt/java-minimal

FROM debian:stretch-slim

RUN mkdir -p /opt/java-minimal 

ENV JAVA_HOME=/opt/java-minimal
ENV PATH="$PATH:$JAVA_HOME/bin"

RUN echo $PATH

COPY --from=builder $JAVA_HOME $JAVA_HOME
COPY --from=builder /tmp/target/ws-reactive-client-0.0.1-SNAPSHOT.jar /root/app.jar

EXPOSE 8081

WORKDIR /root