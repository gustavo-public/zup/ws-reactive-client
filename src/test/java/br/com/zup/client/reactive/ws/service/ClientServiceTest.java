package br.com.zup.client.reactive.ws.service;

import br.com.zup.client.reactive.ws.domain.Address;
import br.com.zup.client.reactive.ws.domain.Client;
import br.com.zup.client.reactive.ws.exception.CpfException;
import br.com.zup.client.reactive.ws.repository.ClientRepository;
import br.com.zup.client.reactive.ws.service.impl.ClientServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;

import static org.mockito.Mockito.*;

public class ClientServiceTest {

    @InjectMocks
    private ClientServiceImpl service;

    @Mock
    private ClientRepository repository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    private Client buildDeFaultClient() {
        return Client.builder()
                .cpf("49921837001")
                .birthDate(LocalDate.of(1986, 1, 2))
                .name("Client 01")
                .address(Address.builder()
                        .neighborhood("Bairro 01")
                        .town("Cidade 01")
                        .complement("Complento")
                        .referencePoint("Referencia")
                        .street("Rua 01")
                        .state("AM")
                        .build())
                .build();
    }

    @Test
    public void shouldSaveClient() {
        Client client = buildDeFaultClient();

        doReturn(Mono.just(client)).when(repository).save(client);
        doReturn(Mono.just(false)).when(repository).existsById(client.getCpf());

        Mono<Client> monoClient = service.add(client);

        StepVerifier.create(monoClient.log())
                .expectNext(client)
                .verifyComplete();

        verify(repository, times(1)).save(client);
        verify(repository, times(1)).existsById(client.getCpf());
    }

    @Test
    public void shouldNotSaveRepetedClient() {
        Client client = buildDeFaultClient();

        doReturn(Mono.just(true)).when(repository).existsById(client.getCpf());

        Mono<Client> monoClient = service.add(client);

        StepVerifier.create(monoClient.log())
                .expectError(RuntimeException.class)
                .verify();

        verify(repository, times(1)).existsById(client.getCpf());
    }

    @Test
    public void shouldNotSaveClientWithoutCpf() {
        Client client = buildDeFaultClient();

        client.setCpf(null);

        doReturn(Mono.just(client)).when(repository).save(client);

        Mono mono = service.add(client);

        StepVerifier.create(mono.log())
                .expectError(CpfException.class)
                .verify();
    }

    @Test
    public void shouldNotSaveClientWithEmptyCpf() {
        Client client = buildDeFaultClient();

        client.setCpf("");

        doReturn(Mono.just(client)).when(repository).save(client);

        Mono mono = service.add(client);

        StepVerifier.create(mono.log())
                .expectError(CpfException.class)
                .verify();
    }

    @Test
    public void shouldNotSaveClientwithoutName() {
        Client client = buildDeFaultClient();

        client.setName(null);

        doReturn(Mono.just(client)).when(repository).save(client);

        Mono mono = service.add(client);

        StepVerifier.create(mono.log())
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    public void shouldNotSaveClientwithNameEmpty() {
        Client client = buildDeFaultClient();

        client.setName("");

        doReturn(Mono.just(client)).when(repository).save(client);

        Mono mono = service.add(client);

        StepVerifier.create(mono.log())
                .expectError(RuntimeException.class)
                .verify();
    }


    @Test
    public void shouldEditClient() {
        Client client = buildDeFaultClient();

        doReturn(Mono.just(client)).when(repository).save(client);
        doReturn(Mono.just(true)).when(repository).existsById(client.getCpf());

        Mono<Client> monoClient = service.edit(client);

        StepVerifier.create(monoClient.log())
                .expectNext(client)
                .verifyComplete();

        verify(repository, times(1)).save(client);
        verify(repository, times(1)).existsById(client.getCpf());
    }

    @Test
    public void shouldNotEditClient() {
        Client client = buildDeFaultClient();

        doReturn(Mono.just(false)).when(repository).existsById(client.getCpf());

        Mono<Client> monoClient = service.edit(client);

        StepVerifier.create(monoClient.log())
                .expectError(RuntimeException.class)
                .verify();

        verify(repository, times(1)).existsById(client.getCpf());
    }

    @Test
    public void shouldNotEditClientWithoutCpf() {
        Client client = buildDeFaultClient();

        client.setCpf(null);

        Mono<Client> monoClient = service.edit(client);

        StepVerifier.create(monoClient.log())
                .expectError(CpfException.class)
                .verify();
    }

    @Test
    public void shouldNotEditClientWithCpfEmpty() {
        Client client = buildDeFaultClient();

        client.setCpf("");

        Mono<Client> monoClient = service.edit(client);

        StepVerifier.create(monoClient.log())
                .expectError(CpfException.class)
                .verify();
    }

    @Test
    public void shouldNotEditClientWithoutName() {
        Client client = buildDeFaultClient();

        client.setName(null);

        doReturn(Mono.just(true)).when(repository).existsById(client.getCpf());
        doReturn(Mono.just(client)).when(repository).save(client);

        Mono<Client> monoClient = service.edit(client);

        StepVerifier.create(monoClient.log())
                .expectError(ConstraintViolationException.class)
                .verify();
    }

    @Test
    public void shouldNotEditClientWithNameEmpty() {
        Client client = buildDeFaultClient();

        client.setName("");

        Mono<Client> monoClient = service.edit(client);

        StepVerifier.create(monoClient.log())
                .expectError(RuntimeException.class)
                .verify();
    }

}
