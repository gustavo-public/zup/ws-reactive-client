package br.com.zup.client.reactive.ws.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    private String number;

    @NotNull(message = "{client.address.street.required}")
    @NotBlank(message = "{client.address.street.notblank}")
    private String street;

    @NotNull(message = "{client.address.neighborhood.required}")
    @NotBlank(message = "{client.address.neighborhood.notblank}")
    private String neighborhood;

    @NotNull(message = "{client.address.town.required}")
    @NotBlank(message = "{client.address.town.notblank}")
    private String town;

    @NotNull(message = "{client.address.state.required}")
    @NotBlank(message = "{client.address.state.notblank}")
    @Length(max = 2, min = 2)
    private String state;

    private String complement;
    private String referencePoint;
}
