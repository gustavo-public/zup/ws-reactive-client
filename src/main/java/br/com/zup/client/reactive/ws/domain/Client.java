package br.com.zup.client.reactive.ws.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Client {

    @Id
    @NotNull(message = "{client.cpf.required}")
    @NotBlank(message = "{client.cpf.notblank}")
    @Length(min = 11, max = 11, message = "{client.cpf.invalid}")
    private String cpf;
    @NotNull(message = "{client.name.required}")
    @NotBlank(message = "{client.name.notblank}")
    private String name;

    private LocalDate birthDate;
    @NotNull(message = "{client.address.required}")
    private Address address;

}
