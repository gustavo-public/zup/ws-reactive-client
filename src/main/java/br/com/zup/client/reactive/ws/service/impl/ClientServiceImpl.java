package br.com.zup.client.reactive.ws.service.impl;

import br.com.zup.client.reactive.ws.domain.Client;
import br.com.zup.client.reactive.ws.repository.ClientRepository;
import br.com.zup.client.reactive.ws.service.ClientService;
import br.com.zup.client.reactive.ws.util.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository repository;

    @Override
    public Mono<Client> add(Client client) {
        return Utilities.ClientValidator.validateClient(client)
                .doOnError(err -> Mono.error(new RuntimeException(err)))
                .flatMap(c -> repository.existsById(c.getCpf()).flatMap(exists -> {
                    if (exists)
                        return Mono.error(new RuntimeException("Cliente já existe"));
                    return repository.save(c);
                }));
    }

    @Override
    public Mono<Client> edit(Client client) {
        return Utilities.ClientValidator.validateClient(client)
                .doOnError(err -> Mono.error(new RuntimeException(err)))
                .flatMap(c -> repository.existsById(c.getCpf()).flatMap(exists -> {
                    if (!exists)
                        return Mono.error(new RuntimeException("Cliente não existe"));
                    return repository.save(c);
                }));
    }

    @Override
    public Flux<Client> findAll() {
        return repository.findAll().log();
    }

    @Override
    public Flux<Client> findByName(String name) {
        return repository.findByNameContainsIgnoreCase(name).log();
    }

    @Override
    public Mono<Client> findByCpf(String cpf) {
        return repository.findById(cpf).log();
    }

    @Override
    public Mono<ServerResponse> delete(String cpf) {
        return repository.deleteById(cpf)
                .log()
                .doOnError(err -> Mono.error(err))
                .flatMap(voidResponse ->
                        repository.existsById(cpf).flatMap(exists ->{
                            if(!exists){
                                return ServerResponse.accepted().build();
                            }
                           return ServerResponse.badRequest().build();
                        })
                       );
    }

}
