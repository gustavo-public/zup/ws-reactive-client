package br.com.zup.client.reactive.ws.util;

import br.com.zup.client.reactive.ws.domain.Address;
import br.com.zup.client.reactive.ws.domain.Client;
import br.com.zup.client.reactive.ws.exception.CpfException;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.InputMismatchException;
import java.util.Set;

public final class Utilities {

    private Utilities() {
    }

    public static class ContraitsValidator {

        private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    }

    public static class ClientValidator {

        public static boolean validateCpf(String cpf) {

            if (cpf == null || cpf.equals("00000000000") || cpf.equals("11111111111") ||
                    cpf.equals("22222222222") || cpf.equals("33333333333") ||
                    cpf.equals("44444444444") || cpf.equals("55555555555") ||
                    cpf.equals("66666666666") || cpf.equals("77777777777") ||
                    cpf.equals("88888888888") || cpf.equals("99999999999") ||
                    (cpf.length() != 11))
                return (false);

            char dig10, dig11;
            int sm, i, r, num, peso;


            try {
// Calculo do 1o. Digito Verificador
                sm = 0;
                peso = 10;
                for (i = 0; i < 9; i++) {

                    num = (int) (cpf.charAt(i) - 48);
                    sm = sm + (num * peso);
                    peso = peso - 1;
                }

                r = 11 - (sm % 11);
                if ((r == 10) || (r == 11))
                    dig10 = '0';
                else dig10 = (char) (r + 48);

// Calculo do 2o. Digito Verificador
                sm = 0;
                peso = 11;
                for (i = 0; i < 10; i++) {
                    num = (int) (cpf.charAt(i) - 48);
                    sm = sm + (num * peso);
                    peso = peso - 1;
                }

                r = 11 - (sm % 11);
                if ((r == 10) || (r == 11))
                    dig11 = '0';
                else dig11 = (char) (r + 48);

// Verifica se os digitos calculados conferem com os digitos informados.
                if ((dig10 == cpf.charAt(9)) && (dig11 == cpf.charAt(10)))
                    return (true);
                else return (false);
            } catch (InputMismatchException erro) {
                return (false);
            }
        }

        public static Mono<Client> validateClient(Client client) {
            if (!validateCpf(client.getCpf()))
                return Mono.error(new CpfException("CPF inválido"));

            Set erros = ContraitsValidator.VALIDATOR.validate(client);

            if (erros != null && !erros.isEmpty())
                return Mono.error(new ConstraintViolationException(erros));

            Set errosDependence = ContraitsValidator.VALIDATOR.validate(client.getAddress());

            if (errosDependence != null && !errosDependence.isEmpty())
                return Mono.error(new ConstraintViolationException(errosDependence));

            return Mono.just(client);
        }
    }
}
