package br.com.zup.client.reactive.ws.controller;

import br.com.zup.client.reactive.ws.domain.Client;
import br.com.zup.client.reactive.ws.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "ws-client/api/client")
public class ClientController {

    @Autowired
    private ClientService service;

    @GetMapping
    public Flux<Client> list() {
        return service.findAll();
    }

    @GetMapping(value = "/name/{name}")
    public Flux<Client> findByName(@Valid @NotBlank @NotNull @PathVariable(required = true) String name) {
        return service.findByName(name);
    }

    @GetMapping(value = "/cpf/{cpf}")
    public Mono<Client> findByCpf(@Valid @PathVariable(required = true) String cpf) {
        return service.findByCpf(cpf);
    }

    @PostMapping
    public Mono<Client> create(@Valid @RequestBody(required = true) Client body) {
        return service.add(body);
    }

    @PutMapping
    public Mono<Client> edit(@Valid @RequestBody(required = true) Client body) {
        return service.edit(body);
    }

    @DeleteMapping(value = "/{cpf}")
    public Mono<ServerResponse> delete(@PathVariable(required = true) String cpf) {
        return service.delete(cpf);
    }


    @ExceptionHandler(WebExchangeBindException.class)
    public Mono<ResponseEntity>error(WebExchangeBindException ex){
        List<String> erros = ex.getAllErrors()
                .stream()
                .map(e -> e.getDefaultMessage())
                .collect(Collectors.toList());
        return Mono.just(ResponseEntity.badRequest().body(erros));
    }
    @ExceptionHandler
    public Mono<ResponseEntity> error(Throwable ex) {
        return Mono.just(ResponseEntity.badRequest().body(ex.getMessage()));
    }

}
