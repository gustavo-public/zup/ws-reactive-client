package br.com.zup.client.reactive.ws.repository;

import br.com.zup.client.reactive.ws.domain.Client;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface ClientRepository extends ReactiveMongoRepository<Client, String> {

    Flux<Client> findByNameContainsIgnoreCase(String name);

}
