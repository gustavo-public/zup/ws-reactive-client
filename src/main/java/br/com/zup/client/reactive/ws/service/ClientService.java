package br.com.zup.client.reactive.ws.service;

import br.com.zup.client.reactive.ws.domain.Client;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


/**
 * Service maintain the {@link Client} entity
 *
 * @author gustavo
 */
public interface ClientService {

    /**
     * Method to create a new {@link Client}
     *
     * @param client the {@link Client} that should be persisted
     * @return {@link Mono} with the {@link Client} persisted entity
     * @see Mono
     * @see Client
     */
    Mono<Client> add(Client client);

    /**
     * Method to edit a existing {@link Client}
     *
     * @param client the {@link Client} that should be edited
     * @return {@link Mono} with the {@link Client} edited
     * @see Mono
     * @see Client
     */
    Mono<Client> edit(Client client);

    /**
     * Method to list all existing {@link Client}
     *
     * @return {@link Flux} with all Clients
     * @see Flux
     * @see Client
     */
    Flux<Client> findAll();

    /**
     * Method to list all existing {@link Client}
     * filtering by name
     *
     * @param name A {@link String} that is the name of the {@Link Client}
     * @return {@link Flux} with all Clients
     * @see Flux
     * @see Client
     */
    Flux<Client> findByName(String name);

    /**
     * Method to search a existing {@link Client}
     * filtering by cpf
     *
     * @param cpf A {@link String} that is the cpf of the {@Link Client}
     * @return {@link Flux} with all Clients
     * @see Mono
     * @see Client
     */
    Mono<Client> findByCpf(String cpf);

    /**
     * Method to delete a existing {@link Client} by cpf
     *
     * @param cpf A {@link String} that is the cpf of the {@Link Client}
     * @return a {@link Mono} with the {@Link ServerResponse} that has the http status code
     * @see Client
     * @see Mono
     * @see ServerResponse
     */
    Mono<ServerResponse> delete(String cpf);
}
