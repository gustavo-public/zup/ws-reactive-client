package br.com.zup.client.reactive.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsReactiveClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(WsReactiveClientApplication.class, args);
    }

}
